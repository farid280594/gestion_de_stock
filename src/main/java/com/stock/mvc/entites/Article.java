package com.stock.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "article")
public class Article implements Serializable{
@Id
@GeneratedValue
private Long idArticle;
private String codeAricle;
private String designation;
private BigDecimal praixUnitaireHT;
private BigDecimal tauxtva;
private BigDecimal praixUnitairTCC;
private String photo;

@ManyToOne
@JoinColumn(name= "idCategory")
private Category category;
public 	Article() {
}
public Long getIdArticle() {
	return idArticle;
}

public void setIdArticle(Long idArticle) {
	this.idArticle = idArticle;
}

public String getCodeAricle() {
	return codeAricle;
}

public void setCodeAricle(String codeAricle) {
	this.codeAricle = codeAricle;
}

public String getDesignation() {
	return designation;
}

public void setDesignation(String desination) {
	this.designation = desination;
}

public BigDecimal getPraixUnitaireHT() {
	return praixUnitaireHT;
}

public void setPraixUnitaireHT(BigDecimal praixUnitaireHT) {
	this.praixUnitaireHT = praixUnitaireHT;
}

public BigDecimal getTauxtva() {
	return tauxtva;
}

public void setTauxtva(BigDecimal tauxtva) {
	this.tauxtva = tauxtva;
}

public BigDecimal getPraixUnitairTCC() {
	return praixUnitairTCC;
}

public void setPraixUnitairTCC(BigDecimal praixUnitairTCC) {
	this.praixUnitairTCC = praixUnitairTCC;
}

public String getPhoto() {
	return photo;
}

public void setPhoto(String photo) {
	this.photo = photo;
}

public Category getCategory() {
	return category;
}

public void setCategory(Category category) {
	this.category = category;
}

}
