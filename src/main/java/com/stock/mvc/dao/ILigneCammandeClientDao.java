package com.stock.mvc.dao;

import com.stock.mvc.entites.LigneCommandeClient;

public interface ILigneCammandeClientDao extends IGenericDao<LigneCommandeClient> {

}
